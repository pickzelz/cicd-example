FROM php:7.2-apache

RUN apt-get -y update && apt-get -y upgrade
RUN apt-get -y install curl

RUN curl -sL https://deb.nodesource.com/setup_16.x | bash -
RUN apt-get install -y nodejs
RUN npm i -g firebase-tools

WORKDIR /var/www/html/