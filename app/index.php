<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <?= "Hello World haha" ?>
    <script type="module">
        // Import the functions you need from the SDKs you need
        import { initializeApp } from "https://www.gstatic.com/firebasejs/9.6.11/firebase-app.js";
        import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.6.11/firebase-analytics.js";
        // TODO: Add SDKs for Firebase products that you want to use
        // https://firebase.google.com/docs/web/setup#available-libraries

        // Your web app's Firebase configuration
        // For Firebase JS SDK v7.20.0 and later, measurementId is optional
        const firebaseConfig = {
            apiKey: "AIzaSyAKiNL_j1MjjNCAV5ZEbWkR9U8iJUYqDRY",
            authDomain: "cicdgitlab-3c493.firebaseapp.com",
            projectId: "cicdgitlab-3c493",
            storageBucket: "cicdgitlab-3c493.appspot.com",
            messagingSenderId: "689114948287",
            appId: "1:689114948287:web:e57be4fe2bb706c0a41c0a",
            measurementId: "G-NVVP20YNSD"
        };

        // Initialize Firebase
        const app = initializeApp(firebaseConfig);
        const analytics = getAnalytics(app);
    </script>
</body>
</html>